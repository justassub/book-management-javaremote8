package lt.sda.academy.presentation;

import lt.sda.academy.util.ScannerUtil;

public class BookChoice {
    public static int selectBookChoice() {
        System.out.println("1.Matyti visas knygas");
        System.out.println("2.Ieškoti knygų pagal pavadinimą arba autorių");
        System.out.println("3.Pridėti knygą");
        System.out.println("4.Atnaujinti knygą");
        System.out.println("5. Ištrinti knygą pagal id");
        return ScannerUtil.SCANNER.nextInt();
    }
}
