package lt.sda.academy.presentation;

import lt.sda.academy.util.ScannerUtil;

public class AuthorChoice {
    public static int selectAuthorsChoices() {
        System.out.println("1. Matyti visus autorius");
        System.out.println("2. Ieškoti autorių pagal vardą arba pavardę ");
        System.out.println("3. Pridėti autorių");
        System.out.println("4. Atnaujinti autorių ");
        System.out.println("5. Ištrinti Autorių pagal id");
        return ScannerUtil.SCANNER.nextInt();
    }

    public static String selectAuthorsName() {
        System.out.println("Prašau įveskite autoriaus vardą(be skaičių): ");
        return ScannerUtil.SCANNER.next();
    }

    public static String selectAuthorsSurname() {
        System.out.println("Prašau įveskite autoriaus Pavardę(be skaičių): ");
        return ScannerUtil.SCANNER.next();
    }

    public static void sayThatFieldWasNotValid(String field) {
        System.out.println("Laukas " + field + " buvo nevalidus.");
    }
}
