package lt.sda.academy.presentation;

import lt.sda.academy.util.ScannerUtil;

public class ApplicationGreeting {

    /**
     * Čia laikysime visą kodą kuris atsakingas už spausdinimą į konsolę arba prašyti paimti informaciją: pvz
     */

    public static int selectMainMeniuWindow() {
        System.out.println("Pasirinkite ką norite daryti: ");
        System.out.println("1. Ar daryti operacijas su Autoriais:");
        System.out.println("2. Ar daryti operacijas su Knygomis:");
        System.out.println("3. Ar daryti operacijas su atsiliepimais:");
        System.out.println("4. Exit");
        return ScannerUtil.SCANNER.nextInt();
    }
}
