package lt.sda.academy.validators;

public class AuthorValidator {
    public boolean isNameOrSurnameValid(String nameOrSurname) {
        for (char letter : nameOrSurname.toCharArray()) {
            if (!Character.isAlphabetic(letter)) {
                return false;
            }
        }
        return true;
    }
}
