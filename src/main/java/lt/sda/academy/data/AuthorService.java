package lt.sda.academy.data;

import lt.sda.academy.model.Author;
import lt.sda.academy.presentation.AuthorChoice;
import lt.sda.academy.presentation.AuthorDataPrinter;
import lt.sda.academy.util.HibernateUtil;
import lt.sda.academy.validators.AuthorValidator;
import org.hibernate.Session;

import javax.persistence.Query;
import java.util.List;

public class AuthorService {

    private AuthorValidator authorValidator = new AuthorValidator();

    public void createNewAuthor() {
        String authorName = AuthorChoice.selectAuthorsName();
        while (!authorValidator.isNameOrSurnameValid(authorName)) {
            AuthorChoice.sayThatFieldWasNotValid("Vardas");
            authorName = AuthorChoice.selectAuthorsName();
        }

        String authorSurname = AuthorChoice.selectAuthorsSurname();
        while (!authorValidator.isNameOrSurnameValid(authorSurname)) {
            AuthorChoice.sayThatFieldWasNotValid("Pavardė");
            authorSurname = AuthorChoice.selectAuthorsName();
        }

        Session newSession = HibernateUtil.getSessionFactory().openSession();

        Author author = new Author();
        author.setName(authorName);
        author.setSurname(authorSurname);
        newSession.save(author);
        newSession.close();
        AuthorDataPrinter.authorSaveSuccessPrint(author);
    }

    public void listAllAuthors() {
        Session session = HibernateUtil.getNextSession();
        Query query = session.createQuery("from Author");
        List<Author> allAuthors = query.getResultList();
        AuthorDataPrinter.printAuthorsPrimaryInfo(allAuthors);
    }
}
