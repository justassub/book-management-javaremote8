package lt.sda.academy;

import lt.sda.academy.data.AuthorService;
import lt.sda.academy.presentation.ApplicationGreeting;
import lt.sda.academy.presentation.AuthorChoice;
import lt.sda.academy.presentation.BookChoice;
import lt.sda.academy.presentation.ReviewChoice;

public class Main {
    private static AuthorService authorService = new AuthorService();

    public static void main(String[] args) {
        int choice = 0;
        while (choice != 4) {

            choice = ApplicationGreeting.selectMainMeniuWindow();
            switch (choice) {
                case 1:
                    int authorChoice = AuthorChoice.selectAuthorsChoices();
                    switch (authorChoice) {
                        case 1:
                            authorService.listAllAuthors();
                            break;
                        case 3:
                            authorService.createNewAuthor();
                            break;
                        default:
                            throw new IllegalArgumentException("We do not support this operation yet");
                    }
                    break;
                case 2:
                    BookChoice.selectBookChoice();
                    break;
                case 3:
                    ReviewChoice.selectReviewChoice();
            }
        }
    }
}
